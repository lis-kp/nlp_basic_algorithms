implements basic nlp algorithms:
1) unigram
2) bigram
3) word segmentation
4) hidden markov model

contains data and python versions of the pseudo code from Graham Neubig nlp tutorial http://www.phontron.com/teaching.php
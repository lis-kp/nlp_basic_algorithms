#Reads a bigram model and calculates entropy on the test set
import math

lambda_1 = 0.95
lambda_2 = 0.95
V = 1000000
W = 0
H = 0

#loads model
probs = {}
model_file = open("bigram.model")
for line in model_file:
    data = line.strip().split("\t")
    w = data[0]
    P = float(data[1])
    probs[w] = P

#test model
test_file = open("../data/wiki-en-test.word")
for line in test_file:
    words = line.strip().split()
    words.append("</s>")
    words.insert(0, "<s>")
    for i in range(1, len(words)):
        if words[i] in probs:
            P1 = lambda_1* float(probs[words[i]]) + float((1-lambda_1))/float(V) #smoothed unigram prob
        else:
            P1 = float((1-lambda_1))/float(V) #smoothed unigram prob

        if words[i-1]+" "+words[i] in probs:
            P2 = lambda_2* float(probs[words[i-1]+" "+words[i]]) + (1 - lambda_2) * P1 #smoothed bigram prob
        else:
            P2 =  (1 - lambda_2) * P1  # smoothed bigram prob

        H += -1* math.log(P2, 2)
        W += 1

print ("entropy = ", float(H)/float(W))
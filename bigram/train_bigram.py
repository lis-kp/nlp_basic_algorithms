# Creates a bigram model

counts = {}
context_counts = {}

training_file = open("../data/wiki-en-train.word")
for line in training_file:
    words = line.strip().split()
    words.append("</s>")
    words.insert(0, "<s>")
    for i in range(1,  len(words)):
        if words[i-1]+" "+words[i] in counts:
            counts[words[i-1]+" "+words[i]] += 1
        else:
            counts[words[i - 1] + " " + words[i]] = 1

        if words[i-1] in context_counts:
            context_counts[words[i-1]] += 1
        else:
            context_counts[words[i - 1]] = 1
        if words[i] in counts:
            counts[words[i]] += 1
        else:
            counts[words[i]] = 1
        if "" in counts:
            context_counts[""] += 1
        else:
            context_counts[""] = 1

model_file = open("bigram.model", "w")
for ngram, count in counts.items():
    words = ngram.split()
    words.pop()
    context = " ".join(words)
    prob = float(counts[ngram])/float(context_counts[context])
    model_file.write(ngram + "\t"+ str(prob))
    model_file.write("\n")

model_file.flush()
model_file.close()

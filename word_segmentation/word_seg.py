#implements viterbi algorithm
import math

#loads unigram model

unigram_probs = {}
model_file = open("../test/04-model.txt")
for line in model_file:
    data = line.strip().split("\t")
    w = data[0]
    P = float(data[1])
    unigram_probs[w] = P

LAMBDA_1 = 0.95
LAMBDA_UNK = 1 - LAMBDA_1
V = 1e210  # Vocabulary size. This value should be set very large for CJK langs!

input_file = open("../test/04-input.txt")


for line in input_file:
    best_edge = {}
    best_score = {}

    #forward step
    line = line.strip()
    best_edge[0] = None
    best_score[0] = 0.
    for  word_end in range(1, len(line)+1):
        best_score[word_end] = 1000000 #set to a very large value
        for word_begin in range(0, word_end):
            word = line[word_begin:word_end] #get the substring
            prob = LAMBDA_UNK / V
            if word in unigram_probs:
                prob += LAMBDA_1*unigram_probs[word]
            my_score = best_score[word_begin] + (-1)*math.log(prob,2)
            if my_score < best_score[word_end]:
                best_score[word_end] = my_score
                best_edge[word_end] = (word_begin, word_end)

    #backward step
    words = []
    next_edge = best_edge[len(best_edge)-1]
    while next_edge != None:
        #add the substring for this edge to the words
        word = line[next_edge[0]:next_edge[1]]
        words.append(word)
        next_edge = best_edge[next_edge[0]]
    words.reverse()
    print(" ".join(words))

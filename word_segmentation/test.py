# Dynamic Programming Python implementation of Coin
# Change problem
def count(v, m, n):
    # table[i] will be storing the number of solutions for
    # value i. We need n+1 rows as the table is constructed
    # in bottom up manner using the base case (n = 0)
    # Initialize all table values as 0
    table = [0 for k in range(n + 1)]

    # Base case (If given value is 0)
    table[0] = 1

    # Pick all coins one by one and update the table[] values
    # after the index greater than or equal to the value of the
    # picked coin
    for i in range(0, m):
        for j in range(v[i], n + 1):
            table[j] += table[j - v[i]]

    return table[n]


# Driver program to test above function
v = [1, 2, 3]
m = len(v)
n = 4
x = count(v, m, n)
print(x)

# This code is contributed by Afzal Ansari
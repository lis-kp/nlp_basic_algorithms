#Reads a unigram model and calculates entropy and coverage for the test set

import math

lambda_1 = 0.95
lambda_unk = 1 - lambda_1 #save some probability for unknown words
V = 1000000 #guess total vocabulary size, including unknown words
W = 0 #total words
H = 0 #entropy
unk = 0 #total unknown words

#loads model
probs = {}
model_file = open("unigram.model")
for line in model_file:
    data = line.strip().split()
    w = data[0]
    P = float(data[1])
    probs[w] = P

#test model
test_file = open("data/wiki-en-test.word")
for line in test_file:
    words = line.strip().split()
    for word in words:
        W += 1
        P = float(lambda_unk)/float(V)
        if word in probs:
            P+= lambda_1 * probs[word]
        else:
            unk += 1
        H += -1* math.log(P, 2)

#print entropy and coverage
print ("entropy = ", float(H)/float(W))
print("coverage = ", (W-unk)/W)


counts = {}
total_count = 0

training_file = open("../data/wiki-ja-train.word")

for line in training_file:
    words = line.strip().split()
    words.append("</s>")
    for word in words:
        if word in counts:
            counts[word] += 1
        else:
            counts[word] = 1
        total_count += 1

    model_file = open("unigram_jp.model", "w")
    for word, count in counts.items():
        prob = float(count)/total_count
        model_file.write(word+" "+str(prob))
        model_file.write("\n")

model_file.flush()
model_file.close()

